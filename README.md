##README.md

PetDetective Test App 
 ```
Project Objectives: This is a demo app that will be used to discover lost pets & report found pets in the local region 
after GPS has been enabled. The app will be tested with focus groups in the region to determine if there is a need in the market 
for such an app and if it meets consumers' demands. If test app is successful, then a revised version will be uploaded to 
Google Play Store.
```
AUTHOR: BREUNA BAINE, PRATYUSHA THUNDENA, TAYLOR MARTINDALE 

ACKNOWLEGMENT: https://developer.android.com/training/basics/firstapp

PREREQUISITES: Android Studio, Gradle, Java Programming 

INSTRUCTIONS:

Download app-debug.apk 


Screenshots:

Splash Screen Screenshot: 

![Webp net-resizeimage](https://user-images.githubusercontent.com/46456051/59644013-75fe1b80-9141-11e9-86c0-5c0f1d19b736.jpg)


The Login Page Screenshot: 


![Webp net-resizeimage (1)](https://user-images.githubusercontent.com/46456051/59644139-0fc5c880-9142-11e9-98ce-a84552d396b4.jpg)


The Signup Page Screenshot: 

![Webp net-resizeimage (2)](https://user-images.githubusercontent.com/46456051/59644320-d80b5080-9142-11e9-8524-ac2b7da952a2.jpg)


The 2 Options (Lost/Found) Page Screenshot: 


![Webp net-resizeimage (3)](https://user-images.githubusercontent.com/46456051/59644485-77c8de80-9143-11e9-8494-62e97e9fff69.jpg)


The Lost Page Screenshot: 


![Webp net-resizeimage (4)](https://user-images.githubusercontent.com/46456051/59644616-12c1b880-9144-11e9-9c4c-e0bc449a2e36.jpg)


Detailed Lost Page Screenshot: 

![Webp net-resizeimage (5)](https://user-images.githubusercontent.com/46456051/59644755-b01cec80-9144-11e9-8486-940c48a8230a.jpg)


Side Menu Page Screenshot:


![Webp net-resizeimage (6)](https://user-images.githubusercontent.com/46456051/59644861-33d6d900-9145-11e9-89ca-2d35d53cceba.jpg)


Edit Post Page Screenshot: 


![Webp net-resizeimage (7)](https://user-images.githubusercontent.com/46456051/59644969-be1f3d00-9145-11e9-8f4f-77f54340a5c8.jpg)


Pet Description Page Screenshot:


![Webp net-resizeimage (8)](https://user-images.githubusercontent.com/46456051/59645066-47367400-9146-11e9-9198-9ce6c7c6524d.jpg)



CONTACT ME:

support@petdetecive.com 
```